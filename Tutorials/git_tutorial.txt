# git tutorial and notes

## basics

* 'git init' to initialize empty master branch in the current directory (=git-repo in current directory)
* 'git status' to update and show list of tracked files
* .gitignore file includes all files and folders to be ignored from the mere possibility of being tracked
* 'git add $NAME' to add entities to the list of things that can be committed to branch
* "staged" means "committed to master branch"
* 'git commit' to save changes from status list to repo 
	* opens changelog in default text editor
	* changelog gets attached to each commit so that one can comment on changes
	* # is used for comments that don't appear in the actual log
	* alternative 'git commit -m "TEXT"' does the same thing and uses TEXT as the corresponding attached comment 
* 'git branch $NAME' creates new branch
* 'git checkout $NAME' change to branch $NAME
* 'git merge $BRANCH' commit changes in Branch to current branch
	* current branch is displayed as [$CurrentBranch] in promt

## setting up ssh authentication for a repo

* needs to be redone for every repo
* account name for ssh access to remote repo is universally called "git" by gitlab and github
	* login via git@gitlab.com:username/repo.git
	* gitlab can have username/intermediate/repo.git, github can't
* show which remotes are currently in use: git remote -v
* we always update the current remote origin with: git remote set-url $origin_name $url_for_ssh_access
	* usually $origin_name is just called  "origin" again, so that an example command is:
		* git remote set-url origin git@gitlab:username/repo.git
			* this maps "origin" to the url "git@gitlab:username/repo.git"
* we can define what $branchUP of the remote repo we want to use as upstream for the current local master branch "master" of the repo: git branch --set-upstream-to $origin/$branchUP master
* recommended procedure:
	* 1) create branch in gitlab in browser
	* 2) "check it out" locally with: 
		* git fetch -a $origin
		* git checkout $branchname
	* 3) then push with git push $origin $branchname

* example for how I made repos work before:
	* 0) git remote add origin git@gitlab.com:rrdTv5_files/repo.git
	* 1) git remote set-url origin git@gitlab.com:rrdTv5_files/repo.git
	* 2) git fetch -a origin ~~ 
	* 3) git branch --set-upstream-to origin/master master~~
	* 4) git push --set-upstream origin master
