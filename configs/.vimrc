set nocompatible
set fileencoding=utf-8
filetype off
set rtp+=/home/uprjfvwmnt/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
"Plugin 'https://github.com/nvie/vim-flake8.git'
call vundle#end()
filetype plugin indent on
set number
set clipboard=unnamedplus
set cursorline
set tabstop=4
set shiftwidth=4
set listchars=tab:,.
set noexpandtab
set colorcolumn=80
set list
set shellcmdflag=-ic
set laststatus=2
set statusline=%F
set statusline+=\ col:\ %c\|\ %b\ \|\ 0x%B
set viminfo='20,<1000,s1000
set incsearch
if has('persistent_undo')
  set undofile
endif
set showcmd
set ttimeoutlen=0
set backupdir=/home/uprjfvwmnt/.vim/backups
set backup
set backupext=.bak
vnoremap // y/\V<C-R>=escape(@",'/\')<CR><CR>
:command A :normal A ✓ <ESC>
:command S :normal A -- <ESC>
:command D :normal A ~~ <ESC>
:command F :normal i % <ESC>
colorscheme default
"colorscheme delek
:command Pypack :normal iimport numpy as np<ESC>oimport matplotlib.pyplot as plt<ESC>oimport scipy as sc<ESC>oimport qutip as qt<ESC>oimport multiprocessing as mp<ESC>ofrom mpl_toolkits.mplot3d import Axes3D<ESC>oplt.ion()<ESC>onp.set_printoptions(suppress=True,precision=2)<ESC>
:command Figures :normal ifig=plt.figure()<ESC>oax1,ax2,ax3,ax4=fig.add_subplot(221),fig.add_subplot(222),fig.add_subplot(223),fig.add_subplot(224)
cnoremap <C-j> <Down>
cnoremap <C-k> <Up>
inoremap <C-j> <ESC>jA
inoremap <C-q> <backspace>
inoremap <C-k> <ESC>kA
nnoremap <space> i<space><esc>
vnoremap <F3> y:tabnew<CR>pgg
nnoremap <F5> :cnext<CR>
nnoremap <F6> :cprev<CR>
nnoremap <F7> gt
nnoremap <F8> gT
nnoremap <C-J> :set hlsearch!<CR>
nnoremap <C-K> I#<esc>
:vnoremap <F10> :!python ~/scripts/python/vimscripts/vimsum.py<CR>
:command TEX :normal !!cat ~/Documents/TeX/tex_template.tex<Return>
:command -range=% Time :normal  !!python ~/scripts/python/vim_timestamps/vim_timestamps.py<Return>
set scrolloff=999
set autoindent

hi Visual ctermbg=cyan ctermfg=red

au BufRead,BufNewFile *.txt   syntax match StrikeoutMatch /.*\~\~/   
au BufRead,BufNewFile *.py   syntax match StrikeoutMatch /\#.*/
hi def  StrikeoutColor   ctermbg=black ctermfg=red
hi link StrikeoutMatch StrikeoutColor

au BufRead,BufNewFile *.txt syntax match AcceptedItem /.*✓/   
hi def  AcceptedItemColor ctermbg=black ctermfg=darkgreen
hi link AcceptedItem AcceptedItemColor

au BufRead,BufNewFile *.txt syntax match FilePathItem /.*\/.*/   
hi def  FilePathItemColor ctermbg=black ctermfg=magenta
hi link FilePathItem FilePathItemColor

au BufRead,BufNewFile *.txt syntax match UnsureItem /.*--/   
hi def  UnsureItemColor ctermbg=black ctermfg=darkyellow
hi link UnsureItem UnsureItemColor

hi Folded ctermfg=green
hi Folded ctermbg=black
hi FoldColumn ctermbg=black
hi FoldColumn ctermfg=green
set foldcolumn=1

hi TabLine term=None cterm=None

function FoldToggle()
if &foldmethod=="manual"
	set foldmethod=indent
	echo "Foldmethod set to indent"
else
	if &foldmethod=="indent"
		set foldmethod=syntax
		echo "Foldmethod set to syntax"
	else
		if &foldmethod=="syntax"
			set foldmethod=manual
			echo "Foldmethod set to manual"
		endif
	endif
endif
endfunction

nnoremap <C-F> :call FoldToggle()<CR>
"abbreviations"
iab __init __init__(self,
iab __name __name__ == "__main__":
iab _in ∈
iab _sum ∑
augroup remember_folds
	autocmd!
	autocmd BufWinLeave * mkview
	autocmd BufWinEnter * silent! loadview
augroup END





augroup commenting_blocks_of_code
	autocmd FileType * let b:comment_leader = (split(&commentstring, '%s')+['//'])[0]
augroup END

" Commenting blocks of code.
" augroup commenting_blocks_of_code
"   autocmd!
"   autocmd FileType c,cpp,java,scala let b:comment_leader = '// '
"   autocmd FileType sh,ruby,python   let b:comment_leader = '# '
"   autocmd FileType conf,fstab       let b:comment_leader = '# '
"   autocmd FileType tex              let b:comment_leader = '% '
"   autocmd FileType mail             let b:comment_leader = '> '
"   autocmd FileType vim              let b:comment_leader = '" '
" augroup END
 noremap <silent> ,cc :<C-B>silent <C-E>s/^/<C-R>=escape(b:comment_leader,'\/')<CR>/<CR>:nohlsearch<CR>
 noremap <silent> ,cu :<C-B>silent <C-E>s/^\V<C-R>=escape(b:comment_leader,'\/')<CR>//e<CR>:nohlsearch<CR>


"Now you can type ,cc to comment a line and ,cu to uncomment a line (works both in normal and visual mode).
autocmd FileType python setlocal noexpandtab
