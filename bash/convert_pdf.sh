#!/bin/bash

convert_pdf()
{
	convert -density 200x200 -quality "$1" -compress jpeg "$2" "$3"
}
