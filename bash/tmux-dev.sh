#!/bin/bash

tmuxdev()
{
	cd ~/scripts/python/
	tmux new-session -d 'vim' "$1"
	tmux split-window -h 'ipython'
	tmux -2 attach-session -d
}
