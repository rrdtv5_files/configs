#!/bin/bash

echo "Enter username on this machine:"
read username
echo "Enter language [Eng/Ger] on this machine:"
read language
if [$language = "Eng"]; then
	mkdir /home/username/Documents/Tutorials
else
	mkdir /home/username/Dokumente/Tutorials
fi

#sudo apt update
#sudo apt upgrade -y
#sudo apt install -y vim tmux w3m mutt xclip xdotool xset okular openssh-server htop ubuntu-restricted-extras build-essential vlc git pandoc kolourpaint gromit-mpx ffmpeg pdftk wget curl grep yes
#sudo apt install -y texlive-full

wget -P /home/username/Downloads https://repo.anaconda.com/archive/Anaconda3-2023.07-1-Linux-x86_64.sh
yes Y|./home/username/Downloads/Anaconda3-2023.07-1-Linux-x86_64.sh
conda activate
conda update conda
conda install -y ipython numpy matplotlib scipy sympy pandas
mkdir /home/username/dotfiles_tmp
cd /home/username/dotfiles_tmp
#git clone *link to my dotfile repo*



echo "Install script has finished - time to reboot!"
