#!/bin/bash

makemp3()
{
	filename="${1%.*}"  
	echo "$filename"
	ffmpeg -i "$1" -f mp3 -ab 192000 -vn "${filename}.mp3"
}
